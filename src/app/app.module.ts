import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import { SliderComponent } from './components/slider/slider.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { PlanComponent } from './components/plan/plan.component';
import { BankComponent } from './components/bank/bank.component';
import { LegalComponent } from './components/legal/legal.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule  } from '@ng-bootstrap/ng-bootstrap';
import { HowItsWorkComponent } from './components/how-its-work/how-its-work.component';
import { AboutCommunityComponent } from './components/about-community/about-community.component';
import { PricingTableComponent } from './components/pricing-table/pricing-table.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterService } from './services/Register.service';





@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    SliderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    PlanComponent,
    BankComponent,
    LegalComponent,
    ContactComponent,
    LoginComponent,
    HowItsWorkComponent,
    AboutCommunityComponent,
    PricingTableComponent,
    DashboardComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
   
  ],
 



  providers: [RegisterService],
  bootstrap: [AppComponent]
})


export class AppModule { }
