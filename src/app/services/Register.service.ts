import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RegisterService {
    constructor(private http: HttpClient) {

    }

    registerUser(userDetails) {
        return this.http.post(`http://admin.rapidoffer.biz/register`, userDetails);
    }

    loginUser(userDetails) {
        return this.http.post(`http://admin.rapidoffer.biz/login`, userDetails);
    }
}