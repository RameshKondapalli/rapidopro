import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/Register.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  registrationForm:  FormGroup;

  constructor(private formBuilder: FormBuilder,private router:Router, private registerService: RegisterService) { }

  ngOnInit() {
 
    this.loginTemplateForm();
    this.registerTemplateForm();
   
  }


  loginTemplateForm(){
    this.loginForm = this.formBuilder.group({
      email : new FormControl(null, [Validators.required]),
      pswd: new FormControl(null, [Validators.required,Validators.pattern('[a-zA-Z0-9@]{6,15}')]),
     
    });
  }
  loginSucess(){
event.preventDefault();
if(this.loginForm.invalid){
  return this.loginForm;
}

     this.registerService.loginUser(this.loginForm.value).subscribe(response => {
       localStorage.setItem('userDetails', JSON.stringify(response));
       this.router.navigate(['/dashboard']);
     })
    
  }
  registerTemplateForm(){
    this.registrationForm = new FormGroup({
       fName: new FormControl(null, [Validators.required,Validators.pattern('[a-zA-z]{1,30}')]),
      lName: new FormControl(null, [Validators.required,Validators.pattern('[a-zA-z]{1,30}')]),
      email: new FormControl(null, [Validators.required]),
      mobile: new FormControl(null, [Validators.required, Validators.pattern('[0-9]{10}')]),
      password: new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z0-9@#$-.]{8,15}')]),
      reference: new FormControl(null, [Validators.required]),

    });
  }

  onRegister(event: Event) {
    event.preventDefault();
    console.log(this.registrationForm.value);
    this.registerService.registerUser(this.registrationForm.value).subscribe(response => {
      console.log(response);

    });

  }
}
